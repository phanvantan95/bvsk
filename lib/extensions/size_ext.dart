import 'package:flutter/material.dart';
import 'package:get/get.dart';

extension SizerExt on double {
  double get h => SizeUtil.height(this);

  double get w => SizeUtil.width(this);

  double get sp => SizeUtil.sp(this);
}

class SizeUtil {
  static const double widthDefault = 428;
  static const double heightDefault = 926;

  static double? _width;
  static double? _height;
  static DeviceType? _deviceType;

  void init() {
    try {
      _width = Get.width;
      _height = Get.height;
      if ((Get.context?.orientation ?? Orientation.portrait) == Orientation.landscape) {
        final double? temp = _width;
        _width = _height;
        _height = temp;
      }
    } catch (exception) {
      _width = widthDefault;
      _height = heightDefault;
    }

    if ((_width ?? widthDefault) < 600) {
      _deviceType = DeviceType.mobile;
    } else {
      _deviceType = DeviceType.tablet;
    }
  }

  static double height(double i) {
    return (_height ?? heightDefault) * i / heightDefault;
  }

  static double width(double i) {
    return (_width ?? widthDefault) * i / widthDefault;
  }

  static double sp(double i) {
    return ((_width ?? widthDefault) / widthDefault) * i * (_deviceType == DeviceType.tablet ? 9.0 / 13.0 : 1.0);
  }

  static DeviceType get deviceType => _deviceType ?? DeviceType.mobile;
}

enum DeviceType {
  mobile,
  tablet,
}
