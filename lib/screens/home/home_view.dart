import 'package:bvsk/screens/contact/contact_view.dart';
import 'package:bvsk/screens/device/device_view.dart';
import 'package:bvsk/screens/prevention/prevention_view.dart';
import 'package:fancy_bottom_navigation/fancy_bottom_navigation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:bvsk/theme/ui_color.dart';

class HomeView extends StatefulWidget {
  const HomeView({Key? key}) : super(key: key);

  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  var tabIndex = 0.obs;

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return Scaffold(
        appBar: PreferredSize(
          preferredSize: const Size.fromHeight(0.0),
          child: AppBar(
            backgroundColor: UIColor.white,
            elevation: 0.0,
            toolbarHeight: 0.0,
            systemOverlayStyle: SystemUiOverlayStyle.dark,
          ),
        ),
        backgroundColor: UIColor.white,
        bottomNavigationBar: FancyBottomNavigation(
          tabs: [
            TabData(
              iconData: Icons.shield,
              title: "Phòng ngừa",
            ),
            TabData(
              iconData: Icons.connect_without_contact,
              title: "Kết nối",
            ),
            TabData(
              iconData: Icons.contact_support,
              title: "Liên lạc",
            ),
          ],
          initialSelection: tabIndex.value,
          onTabChangedListener: onTabChanged,
          activeIconColor: UIColor.white,
          inactiveIconColor: UIColor.green,
          circleColor: UIColor.green,
          textColor: UIColor.green,
        ),
        body: tabIndex.value == 0
            ? const SingleChildScrollView(
                physics: BouncingScrollPhysics(),
                child: PreventionView(),
              )
            : tabIndex.value == 1
                ? const SingleChildScrollView(
                    physics: BouncingScrollPhysics(),
                    child: DeviceList(),
                  )
                : const ContactView(),
      );
    });
  }

  onTabChanged(int position) {
    tabIndex.value = position;
  }
}
