import 'package:bvsk/extensions/size_ext.dart';
import 'package:bvsk/screens/device/device_details_view.dart';
import 'package:bvsk/theme/ui_text_style.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DeviceList extends StatefulWidget {
  const DeviceList({Key? key}) : super(key: key);

  @override
  State<DeviceList> createState() => _DeviceListState();
}

class _DeviceListState extends State<DeviceList> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0.w),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          SizedBox(
            height: 30.0.h,
          ),
          Image(
            height: 50.0.h,
            image: const AssetImage('assets/logo.png'),
            fit: BoxFit.contain,
          ),
          SizedBox(
            height: 30.0.h,
          ),
          Text(
            "Thiết bị của tôi",
            style: UITextStyle.header,
          ),
          SizedBox(
            height: 20.0.h,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 16.0.w, vertical: 16.0.h),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0.h),
              color: Colors.grey[100],
            ),
            child: Column(
              children: [
                GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () {
                    Get.to(() => const DeviceDetailsView());
                  },
                  child: generateDevice(
                    "RESMED AIRMINI (1)",
                    "Đã kết nối",
                  ),
                ),
                SizedBox(height: 8.0.h),
                generateDevice("RESMED AIRMINI (2)", "Không kết nối"),
              ],
            ),
          ),
          SizedBox(
            height: 20.0.h,
          ),
          Row(
            children: [
              Text(
                "Các thiết bị khác",
                style: UITextStyle.header,
              ),
              SizedBox(
                width: 8.0.w,
              ),
              SizedBox(
                width: 24.0.h,
                height: 24.0.h,
                child: const CircularProgressIndicator(),
              ),
            ],
          ),
          SizedBox(
            height: 20.0.h,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 16.0.w, vertical: 16.0.h),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0.h),
              color: Colors.grey[100],
            ),
            child: Column(
              children: [
                generateDevice("RESMED AIRMINI (3)", "Kết nối"),
                SizedBox(height: 8.0.h),
                generateDevice("RESMED AIRMINI (4)", "Kết nối"),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget generateDevice(String title, String status) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              title,
              style: UITextStyle.mediumBlack_16_w400,
            ),
            Row(
              children: [
                Text(
                  status,
                  style: UITextStyle.gray_16_w400,
                ),
                Icon(
                  Icons.info_outline,
                  color: Colors.blue,
                  size: 24.0.h,
                ),
              ],
            )
          ],
        ),
        SizedBox(
          height: 8.0.h,
        ),
        Divider(
          height: 2.0.h,
        ),
      ],
    );
  }
}
