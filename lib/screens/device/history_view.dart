import 'package:bvsk/extensions/size_ext.dart';
import 'package:bvsk/shared/widget/app_bar_widget.dart';
import 'package:bvsk/theme/ui_text_style.dart';
import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:intl/intl.dart' show DateFormat;

class HistoryView extends StatefulWidget {
  const HistoryView({Key? key}) : super(key: key);

  @override
  State<HistoryView> createState() => _HistoryViewState();
}

class _HistoryViewState extends State<HistoryView> {
  List<ChartSampleData>? chartData;

  @override
  void initState() {
    super.initState();
    chartData = <ChartSampleData>[
      ChartSampleData(x: DateTime(2022, 4, 8), y: 0, yValue: 0, secondSeriesYValue: 0, thirdSeriesYValue: 0.5),
      ChartSampleData(x: DateTime(2022, 4, 9), y: 0, yValue: 0, secondSeriesYValue: 0, thirdSeriesYValue: 0.8),
      ChartSampleData(x: DateTime(2022, 4, 10), y: 0, yValue: 0, secondSeriesYValue: 0, thirdSeriesYValue: 0.4),
      ChartSampleData(x: DateTime(2022, 4, 11), y: 0, yValue: 0, secondSeriesYValue: 0, thirdSeriesYValue: 0.5),
      ChartSampleData(x: DateTime(2022, 4, 12), y: 0, yValue: 0, secondSeriesYValue: 0, thirdSeriesYValue: 0.6),
      ChartSampleData(x: DateTime(2022, 4, 13), y: 0, yValue: 0, secondSeriesYValue: 0, thirdSeriesYValue: 0.7),
      ChartSampleData(x: DateTime(2022, 4, 14), y: 0, yValue: 0, secondSeriesYValue: 0, thirdSeriesYValue: 0.9),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(
        context,
        title: "Lịch sử",
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.0.w),
        child: SingleChildScrollView(
          child: Column(
            children: [
              _buildCartesianLegendOptionsChart(),
              _buildHistory("14/04/2022", "Bạn đã thực hiện xông 30 phút"),
              _buildHistory("13/04/2022", "Bạn đã thực hiện xông 48 phút"),
              _buildHistory("12/04/2022", "Bạn đã thực hiện xông 24 phút"),
              _buildHistory("11/04/2022", "Bạn đã thực hiện xông 30 phút"),
              _buildHistory("10/04/2022", "Bạn đã thực hiện xông 42 phút"),
              _buildHistory("09/04/2022", "Bạn đã thực hiện xông 48 phút"),
              _buildHistory("08/04/2022", "Bạn đã thực hiện xông 55 phút"),
            ],
          ),
        ),
      ),
    );
  }

  /// Returns the stacked line chart with various legedn modification options.
  SfCartesianChart _buildCartesianLegendOptionsChart() {
    return SfCartesianChart(
      plotAreaBorderWidth: 0,
      title: ChartTitle(text: 'Thời gian sử dụng'),

      /// Legend and its options for cartesian chart.
      legend: Legend(
        isVisible: false,
        position: LegendPosition.top,
        // offset: enableFloatingLegend ? Offset(_xOffset, _yOffset) : null,
        overflowMode: LegendItemOverflowMode.wrap,
        toggleSeriesVisibility: true,
        backgroundColor: Colors.white.withOpacity(0.5),
        borderColor: Colors.black.withOpacity(0.5),
        borderWidth: 1,
      ),

      primaryXAxis: DateTimeAxis(
        majorGridLines: const MajorGridLines(width: 0),
        interval: 1,
        intervalType: DateTimeIntervalType.days,
        labelFormat: '{value}',
        dateFormat: DateFormat("dd/MM"),
      ),
      primaryYAxis: NumericAxis(
        axisLine: const AxisLine(width: 0),
        labelFormat: '{value}h',
        interval: 0.2,
        majorTickLines: const MajorTickLines(size: 0),
        maximum: 1,
      ),
      series: _getStackedAreaSeries(),
      tooltipBehavior: TooltipBehavior(enable: true),
    );
  }

  List<StackedAreaSeries<ChartSampleData, DateTime>> _getStackedAreaSeries() {
    return <StackedAreaSeries<ChartSampleData, DateTime>>[
      StackedAreaSeries<ChartSampleData, DateTime>(
          animationDuration: 2500,
          dataSource: chartData!,
          xValueMapper: (ChartSampleData sales, _) => sales.x as DateTime,
          yValueMapper: (ChartSampleData sales, _) => sales.thirdSeriesYValue,
          name: 'Sử dụng')
    ];
  }

  Widget _buildHistory(String title, String value) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: 8.0.h,
        ),
        RichText(
          text: TextSpan(text: "$title: ", style: UITextStyle.mediumBlack_16_w700, children: [
            TextSpan(
              text: value,
              style: UITextStyle.mediumBlack_16_w400,
            ),
          ]),
        ),
        SizedBox(
          height: 8.0.h,
        ),
        const Divider(),
      ],
    );
  }
}

///Chart sample data
class ChartSampleData {
  /// Holds the datapoint values like x, y, etc.,
  ChartSampleData({
    this.x,
    this.y,
    this.xValue,
    this.yValue,
    this.secondSeriesYValue,
    this.thirdSeriesYValue,
    this.pointColor,
    this.size,
    this.text,
    this.open,
    this.close,
    this.low,
    this.high,
    this.volume,
  });

  /// Holds x value of the datapoint
  final dynamic x;

  /// Holds y value of the datapoint
  final num? y;

  /// Holds x value of the datapoint
  final dynamic xValue;

  /// Holds y value of the datapoint
  final num? yValue;

  /// Holds y value of the datapoint(for 2nd series)
  final num? secondSeriesYValue;

  /// Holds y value of the datapoint(for 3nd series)
  final num? thirdSeriesYValue;

  /// Holds point color of the datapoint
  final Color? pointColor;

  /// Holds size of the datapoint
  final num? size;

  /// Holds datalabel/text value mapper of the datapoint
  final String? text;

  /// Holds open value of the datapoint
  final num? open;

  /// Holds close value of the datapoint
  final num? close;

  /// Holds low value of the datapoint
  final num? low;

  /// Holds high value of the datapoint
  final num? high;

  /// Holds open value of the datapoint
  final num? volume;
}
