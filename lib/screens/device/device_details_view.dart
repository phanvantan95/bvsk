import 'dart:math';

import 'package:bvsk/extensions/size_ext.dart';
import 'package:bvsk/screens/device/history_view.dart';
import 'package:bvsk/shared/widget/app_bar_widget.dart';
import 'package:bvsk/theme/ui_color.dart';
import 'package:bvsk/theme/ui_text_style.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DeviceDetailsView extends StatefulWidget {
  const DeviceDetailsView({Key? key}) : super(key: key);

  @override
  State<DeviceDetailsView> createState() => _DeviceDetailsViewState();
}

class _DeviceDetailsViewState extends State<DeviceDetailsView> with TickerProviderStateMixin {
  late AnimationController controller;
  late AnimationController airController;
  late AnimationController airBehindController;
  Random random = Random();
  var posA = 0.0.obs;
  var posB = 0.0.obs;
  var posC = 0.0.obs;
  var posD = 0.0.obs;
  var posE = 0.0.obs;
  var posF = 0.0.obs;
  var posG = 0.0.obs;
  var posH = 0.0.obs;
  var isOn = false.obs;
  var optionChoose = 0.obs;
  var temp = 30.0.obs;
  var amp = 60.0.obs;
  var hour = 0.0.obs;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
      duration: const Duration(seconds: 2),
      vsync: this,
    );
    repeatController();
    airController = AnimationController(
      duration: const Duration(seconds: 3),
      vsync: this,
    );
    airController.repeat(reverse: true);
    airBehindController = AnimationController(
      duration: const Duration(seconds: 3),
      vsync: this,
    );
    airBehindController.repeat(reverse: true);
  }

  void repeatController() {
    controller.reset();
    controller.forward().then((value) {
      posA.value = random.nextDouble() * (random.nextBool() ? -1 : 1);
      posB.value = random.nextDouble() * (random.nextBool() ? -1 : 1);
      posC.value = random.nextDouble() * (random.nextBool() ? -1 : 1);
      posD.value = random.nextDouble() * (random.nextBool() ? -1 : 1);
      posE.value = random.nextDouble() * (random.nextBool() ? -1 : 1);
      posF.value = random.nextDouble() * (random.nextBool() ? -1 : 1);
      posG.value = random.nextDouble() * (random.nextBool() ? -1 : 1);
      posH.value = random.nextDouble() * (random.nextBool() ? -1 : 1);
      repeatController();
    });
  }

  @override
  void dispose() {
    controller.dispose();
    airController.dispose();
    airBehindController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(
        context,
        title: "RESMED AIRMINI",
      ),
      body: Obx(() {
        return SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 16.0.w),
            child: Column(
              children: [
                Align(
                  alignment: Alignment.center,
                  child: SizedBox(
                    width: 250.0.w,
                    height: 250.0.h,
                    child: Stack(
                      children: [
                        isOn.value
                            ? Stack(
                                children: [
                                  SlideTransition(
                                    position: Tween<Offset>(
                                      begin: Offset(posA.value, posB.value),
                                      end: Offset.zero,
                                    ).animate(
                                      CurvedAnimation(
                                        parent: controller,
                                        curve: Curves.easeInCubic,
                                      ),
                                    ),
                                    child: buildDot(),
                                  ),
                                  SlideTransition(
                                    position: Tween<Offset>(
                                      begin: Offset(posC.value, posD.value),
                                      end: Offset.zero,
                                    ).animate(
                                      CurvedAnimation(
                                        parent: controller,
                                        curve: Curves.easeInCubic,
                                      ),
                                    ),
                                    child: buildDot(),
                                  ),
                                  SlideTransition(
                                    position: Tween<Offset>(
                                      begin: Offset(posE.value, posF.value),
                                      end: Offset.zero,
                                    ).animate(
                                      CurvedAnimation(
                                        parent: controller,
                                        curve: Curves.easeInCubic,
                                      ),
                                    ),
                                    child: buildDot(),
                                  ),
                                  SlideTransition(
                                    position: Tween<Offset>(
                                      begin: Offset(posG.value, posH.value),
                                      end: Offset.zero,
                                    ).animate(
                                      CurvedAnimation(
                                        parent: controller,
                                        curve: Curves.easeInCubic,
                                      ),
                                    ),
                                    child: buildDot(),
                                  ),
                                  SlideTransition(
                                    position: Tween<Offset>(
                                      begin: Offset(posB.value, posC.value),
                                      end: Offset.zero,
                                    ).animate(
                                      CurvedAnimation(
                                        parent: controller,
                                        curve: Curves.easeInCubic,
                                      ),
                                    ),
                                    child: buildDot(),
                                  ),
                                  SlideTransition(
                                    position: Tween<Offset>(
                                      begin: Offset(posA.value, posD.value),
                                      end: Offset.zero,
                                    ).animate(
                                      CurvedAnimation(
                                        parent: controller,
                                        curve: Curves.easeInCubic,
                                      ),
                                    ),
                                    child: buildDot(),
                                  ),
                                ],
                              )
                            : Container(),
                        Center(
                          child: SizedBox(
                            width: 170.0.h,
                            height: 170.0.h,
                            child: Stack(
                              children: [
                                RotationTransition(
                                  turns: Tween(begin: 0.1, end: 0.9).animate(airController),
                                  child: Container(
                                    width: 190.0.h,
                                    height: 170.0.h,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(75.0.h),
                                      color: isOn.value ? Colors.green[100] : Colors.grey[100],
                                    ),
                                  ),
                                ),
                                RotationTransition(
                                  turns: Tween(begin: 0.9, end: 0.1).animate(airBehindController),
                                  child: Container(
                                    width: 170.0.h,
                                    height: 190.0.h,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(75.0.h),
                                      color: isOn.value ? Colors.green[200] : Colors.grey[200],
                                    ),
                                  ),
                                ),
                                Center(
                                  child: Container(
                                    width: 150.0.h,
                                    height: 150.0.h,
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: isOn.value ? UIColor.green : Colors.grey,
                                    ),
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        Text(
                                          "Bạn đã xông",
                                          style: UITextStyle.white_14_w400,
                                        ),
                                        Text(
                                          "10",
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 50.0.h,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                        Text(
                                          "Phút",
                                          style: UITextStyle.white_14_w400,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                isOn.value
                    ? Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Column(
                            children: [
                              Text(
                                "30",
                                style: UITextStyle.header,
                              ),
                              Text(
                                "Nhiệt độ (°C)",
                                style: UITextStyle.gray_16_w400,
                              ),
                            ],
                          ),
                          SizedBox(
                            width: 16.0.w,
                          ),
                          Container(
                            height: 60.0.h,
                            width: 2.0.w,
                            color: Colors.grey,
                          ),
                          SizedBox(
                            width: 16.0.w,
                          ),
                          Column(
                            children: [
                              Text(
                                "68",
                                style: UITextStyle.header,
                              ),
                              Text(
                                "Độ ẩm (%)",
                                style: UITextStyle.gray_16_w400,
                              ),
                            ],
                          ),
                        ],
                      )
                    : Container(),
                SizedBox(
                  height: 16.0.h,
                ),
                Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: 16.0.w,
                    vertical: 24.0.h,
                  ),
                  margin: EdgeInsets.symmetric(vertical: 8.0.h),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0.h),
                    border: Border.all(color: UIColor.lightGrayBorder),
                    color: UIColor.white,
                    boxShadow: const [
                      BoxShadow(
                        color: UIColor.lightShadeGray,
                        offset: Offset(1, 2),
                        spreadRadius: 1,
                      ),
                    ],
                  ),
                  child: GestureDetector(
                    onTap: () {
                      isOn.value = !isOn.value;
                    },
                    child: Row(
                      children: [
                        Container(
                          width: 40.0.h,
                          height: 40.0.h,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: isOn.value ? Colors.red : Colors.green,
                          ),
                          child: Icon(
                            Icons.power_settings_new,
                            color: Colors.white,
                            size: 32.0.h,
                          ),
                        ),
                        SizedBox(
                          width: 8.0.w,
                        ),
                        Text(
                          isOn.value ? "Tắt" : "Bật",
                          style: UITextStyle.mediumBlack_16_w400,
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: 16.0.w,
                    vertical: 24.0.h,
                  ),
                  margin: EdgeInsets.symmetric(vertical: 8.0.h),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0.h),
                    border: Border.all(color: UIColor.lightGrayBorder),
                    color: UIColor.white,
                    boxShadow: const [
                      BoxShadow(
                        color: UIColor.lightShadeGray,
                        offset: Offset(1, 2),
                        spreadRadius: 1,
                      ),
                    ],
                  ),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          GestureDetector(
                            onTap: () {
                              optionChoose.value = 0;
                            },
                            child: generateOption("Tự động", Icons.brightness_auto_rounded, isChoose: optionChoose.value == 0),
                          ),
                          GestureDetector(
                            onTap: () {
                              optionChoose.value = 1;
                            },
                            child: generateOption("Ban đêm", Icons.nightlight_round, isChoose: optionChoose.value == 1),
                          ),
                          GestureDetector(
                            onTap: () {
                              optionChoose.value = 2;
                              final Widget dialog = Dialog(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5),
                                ),
                                elevation: 0,
                                backgroundColor: Colors.transparent,
                                child: Container(
                                  width: Get.width - 100.0.w,
                                  height: 300.0.h,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10.0.h),
                                    border: Border.all(color: UIColor.lightGrayBorder),
                                    color: UIColor.white,
                                  ),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      generateLevel("Mức 1", Colors.green[300]!),
                                      generateLevel("Mức 2", Colors.green[500]!),
                                      generateLevel("Mức 3", Colors.green[700]!),
                                    ],
                                  ),
                                ),
                              );
                              Get.dialog(dialog);
                            },
                            child: generateOption("Mức", Icons.fast_forward_rounded, isChoose: optionChoose.value == 2),
                          ),
                          GestureDetector(
                            onTap: () {
                              optionChoose.value = 3;
                            },
                            child: generateOption("Thủ công", Icons.handyman, isChoose: optionChoose.value == 3),
                          ),
                        ],
                      ),
                      if (optionChoose.value == 3)
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: 24.0.h,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Text(
                                  "Thời gian (h)",
                                  style: UITextStyle.mediumBlack_14_w700,
                                ),
                                SizedBox(
                                  width: 24.0.w,
                                ),
                                Expanded(
                                  child: Row(
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: [
                                      GestureDetector(
                                        onTap: () {
                                          if (hour.value > 0) {
                                            hour.value--;
                                          }
                                        },
                                        child: Container(
                                          height: 32.0.h,
                                          width: 32.0.h,
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(4.0.h),
                                            border: Border.all(color: UIColor.secondaryColor),
                                          ),
                                          child: Icon(
                                            Icons.remove,
                                            size: 16.0.h,
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        width: 10.0.w,
                                      ),
                                      SizedBox(
                                        width: 70.0.w,
                                        child: Center(
                                          child: Text(
                                            "${hour.value.toInt()}",
                                            style: UITextStyle.mediumBlack_22_w700,
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        width: 10.0.w,
                                      ),
                                      GestureDetector(
                                        onTap: () {
                                          hour.value++;
                                        },
                                        child: Container(
                                          height: 32.0.h,
                                          width: 32.0.h,
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(4.0.h),
                                            border: Border.all(color: UIColor.secondaryColor),
                                          ),
                                          child: Icon(
                                            Icons.add,
                                            size: 16.0.h,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                            SizedBox(
                              height: 16.0.h,
                            ),
                            Text(
                              "Nhiệt độ (°C)",
                              style: UITextStyle.mediumBlack_14_w700,
                            ),
                            Slider(
                              value: temp.value,
                              max: 70,
                              divisions: 14,
                              label: temp.value.toString(),
                              onChanged: (double value) {
                                setState(() {
                                  temp.value = value;
                                });
                              },
                            ),
                            Text(
                              "Độ ẩm (%)",
                              style: UITextStyle.mediumBlack_14_w700,
                            ),
                            Slider(
                              value: amp.value,
                              max: 100,
                              divisions: 10,
                              label: amp.value.toString(),
                              onChanged: (double value) {
                                setState(() {
                                  amp.value = value;
                                });
                              },
                            ),
                          ],
                        )
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: 16.0.w,
                    vertical: 24.0.h,
                  ),
                  margin: EdgeInsets.symmetric(vertical: 8.0.h),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0.h),
                    border: Border.all(color: UIColor.lightGrayBorder),
                    color: UIColor.white,
                    boxShadow: const [
                      BoxShadow(
                        color: UIColor.lightShadeGray,
                        offset: Offset(1, 2),
                        spreadRadius: 1,
                      ),
                    ],
                  ),
                  child: GestureDetector(
                    onTap: () {
                      Get.to(() => const HistoryView());
                    },
                    child: Row(
                      children: [
                        Container(
                          width: 40.0.h,
                          height: 40.0.h,
                          decoration: const BoxDecoration(
                            shape: BoxShape.circle,
                            color: Colors.orange,
                          ),
                          child: Icon(
                            Icons.history_edu,
                            color: Colors.white,
                            size: 32.0.h,
                          ),
                        ),
                        SizedBox(
                          width: 8.0.w,
                        ),
                        Text(
                          "Lịch sử",
                          style: UITextStyle.mediumBlack_16_w400,
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      }),
    );
  }

  Widget buildDot() {
    return Align(
      alignment: Alignment.center,
      child: Icon(
        Icons.circle,
        color: Colors.green,
        size: 8.0.h,
      ),
    );
  }

  Widget generateOption(String title, IconData icon, {isChoose = false}) {
    return Column(
      children: [
        Container(
          width: 50.0.h,
          height: 50.0.h,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            border: Border.all(color: isChoose ? Colors.green : UIColor.gray),
            color: isChoose ? Colors.green : Colors.white,
          ),
          child: Icon(
            icon,
            color: isChoose ? Colors.white : UIColor.gray,
            size: 24.0.h,
          ),
        ),
        SizedBox(
          height: 8.0.h,
        ),
        Text(
          title,
          style: isChoose ? UITextStyle.green_16_w400 : UITextStyle.gray_16_w400,
        ),
      ],
    );
  }

  Widget generateLevel(String title, Color color) {
    return GestureDetector(
      onTap: () {
        Get.back();
      },
      child: Container(
        width: 250.0.w,
        padding: EdgeInsets.symmetric(
          horizontal: 16.0.w,
          vertical: 16.0.h,
        ),
        margin: EdgeInsets.symmetric(vertical: 8.0.h),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.0.h),
          border: Border.all(color: UIColor.lightGrayBorder),
          color: color,
          boxShadow: const [
            BoxShadow(
              color: UIColor.lightShadeGray,
              offset: Offset(1, 2),
              spreadRadius: 1,
            ),
          ],
        ),
        child: Center(
          child: Text(
            title,
            style: UITextStyle.white_18_w700,
          ),
        ),
      ),
    );
  }
}
