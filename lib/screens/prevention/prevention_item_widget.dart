import 'package:bvsk/extensions/size_ext.dart';
import 'package:bvsk/theme/ui_color.dart';
import 'package:bvsk/theme/ui_text_style.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PreventionItemWidget extends StatelessWidget {
  final String? title;
  final String? content;
  final String? image;

  const PreventionItemWidget({this.title, this.content, this.image, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width,
      margin: EdgeInsets.symmetric(horizontal: 12.0.w, vertical: 12.0.h),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(16),
        color: Colors.white,
        boxShadow: [
          BoxShadow(color: UIColor.primaryColor, blurRadius: 4.0.h, spreadRadius: 0.0.h),
        ],
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 8.0.w, vertical: 12.0.h),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              title ?? '',
              style: UITextStyle.header_small,
            ),
            SizedBox(
              height: 8.0.h,
            ),
            Text(
              content ?? '',
              style: UITextStyle.body,
            ),
            SizedBox(
              height: 8.0.h,
            ),
            Image(
              height: 60.0.h,
              image: AssetImage(image ?? ''),
              fit: BoxFit.contain,
            ),
          ],
        ),
      ),
    );
  }
}
