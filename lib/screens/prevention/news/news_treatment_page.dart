import 'package:bvsk/extensions/size_ext.dart';
import 'package:bvsk/screens/prevention/news/news_detail_manual_page.dart';
import 'package:bvsk/screens/prevention/news/news_warning_page.dart';
import 'package:bvsk/screens/prevention/news_item_widget.dart';
import 'package:bvsk/theme/ui_color.dart';
import 'package:bvsk/theme/ui_text_style.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class NewsTreatmentPage extends StatelessWidget {
  const NewsTreatmentPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Stack(
              children: [
                Hero(
                  tag: "assets/treatment.png",
                  child: Image(
                    height: 300.0.h,
                    width: double.infinity,
                    image: const AssetImage("assets/treatment.png"),
                    fit: BoxFit.fill,
                  ),
                ),
                Container(
                  height: 150.0.h,
                  width: double.infinity,
                  decoration: const BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [
                        UIColor.mediumBlack,
                        Colors.transparent,
                      ],
                    ),
                  ),
                ),
                Positioned(
                  top: MediaQuery.of(context).viewPadding.top,
                  left: 16.0.w,
                  child: Row(
                    children: [
                      IconButton(
                        onPressed: () => Get.back(),
                        icon: Icon(
                          Icons.arrow_back_ios,
                          color: UIColor.white,
                          size: 24.0.h,
                        ),
                      ),
                      SizedBox(
                        width: 4.0.w,
                      ),
                      Text(
                        "Hướng dẫn điều trị cho F0",
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: UITextStyle.white_22_w700,
                      )
                    ],
                  ),
                )
              ],
            ),
            SizedBox(
              height: 16.0.h,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 16.0.w),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Số ca mắc COVID-19 trên cả nước đang gia tăng trong thời gần gây, theo đó F0 điều trị tại nhà cũng tăng tại nhiều địa phương. Dưới đây là những thông tin F0 điều trị tại nhà cần biết.",
                    style: UITextStyle.mediumBlack_16_w400,
                  ),
                  SizedBox(
                    height: 4.0.h,
                  ),
                  Text(
                    "1. F0 nào được điều trị tại nhà?",
                    style: UITextStyle.mediumBlack_18_w700,
                  ),
                  SizedBox(
                    height: 4.0.h,
                  ),
                  Text(
                    "- Người mắc COVID-19 (được khẳng định nhiễm SARS-CoV-2 bằng xét nghiệm realtime RT-PCR hoặc test nhanh kháng nguyên theo quy định hiện hành) không có triệu chứng lâm sàng; hoặc có triệu chứng lâm sàng ở mức độ nhẹ: như sốt, ho khan, đau họng, nghẹt mũi, mệt mỏi, đau đầu, đau mỏi cơ, tê lưỡi; tiêu chảy, chảy mũi, mất mùi, mất vị.",
                    style: UITextStyle.mediumBlack_16_w400,
                  ),
                  SizedBox(
                    height: 4.0.h,
                  ),
                  Text(
                    "- Người mắc COVID-19 không có các dấu hiệu của viêm phổi hoặc thiếu oxy; nhịp thở < 20 lần/phút; SpO2 > 96% khi thở khí trời; không có thở bất thường như thở rên, rút lõm lồng ngực, phập phồng cánh mũi, thở khò khè, thở rít ở thì hít vào.",
                    style: UITextStyle.mediumBlack_16_w400,
                  ),
                  SizedBox(
                    height: 4.0.h,
                  ),
                  Text(
                    "- Người mắc COVID-19 không mắc bệnh nền, hoặc có bệnh nền nhưng đang được điều trị ổn định.",
                    style: UITextStyle.mediumBlack_16_w400,
                  ),
                  SizedBox(
                    height: 4.0.h,
                  ),
                  Text(
                    "2. Những việc F0 cần làm để theo dõi sức khỏe hàng ngày",
                    style: UITextStyle.mediumBlack_18_w700,
                  ),
                  SizedBox(
                    height: 4.0.h,
                  ),
                  Text(
                    "- Thời gian: 2 lần/ngày vào buổi sáng và buổi chiều hoặc khi có các dấu hiệu, triệu chứng cần chuyển viện cấp cứu, điều trị.",
                    style: UITextStyle.mediumBlack_16_w400,
                  ),
                  SizedBox(
                    height: 4.0.h,
                  ),
                  Text(
                    """- Nội dung: Chỉ số(nhịp thở, mạch, nhiệt độ, SpO2 và huyết áp), Các triệu chứng(mệt mỏi, ho, ho ra đờm, ớn lạnh/gai rét, viêm kết mạc (mắt đỏ), mất vị giác hoặc khứu giác, tiêu chảy (phân lỏng/đi ngoài); Ho ra máu, thở dốc hoặc khó thở, đau tức ngực kéo dài, lơ mơ, không tỉnh táo), Các triệu chứng khác(Đau họng, nhức đầu, chóng mặt, chán ăn, buồn nôn và nôn, đau nhức cơ,)""",
                    style: UITextStyle.mediumBlack_16_w400,
                  ),
                  SizedBox(
                    height: 4.0.h,
                  ),
                  Text(
                    "3. F0 điều trị tại nhà theo dõi nhịp thở thế nào?",
                    style: UITextStyle.mediumBlack_18_w700,
                  ),
                  SizedBox(
                    height: 4.0.h,
                  ),
                  Text(
                    """- Đối với người lớn: nhịp thở ≥ 20 lần/phút; Đối với trẻ em từ 1 đến dưới 5 tuổi: nhịp thở ≥ 40 lần/phút;
    - Trẻ từ 5 – dưới 12 tuổi: nhịp thở ≥ 30 lần/phút cần báo ngay với cơ sở quản lý người nhiễm COVID-19 tại nhà; trạm y tế xã, phường; hoặc trạm y tế lưu động, Trung tâm vận chuyển cấp cứu… để được xử trí cấp cứu và chuyển viện kịp thời.""",
                    style: UITextStyle.mediumBlack_16_w400,
                  ),
                  SizedBox(
                    height: 4.0.h,
                  ),
                  Text(
                    "4. Danh mục thuốc điều trị ngoại trú cho F0 tại nhà",
                    style: UITextStyle.mediumBlack_18_w700,
                  ),
                  SizedBox(
                    height: 4.0.h,
                  ),
                  Image(
                    height: 700.0.h,
                    width: double.infinity,
                    image: const AssetImage("assets/ic_medicine.png"),
                    fit: BoxFit.fill,
                  ),
                  SizedBox(
                    height: 4.0.h,
                  ),
                  Text(
                    "5. F0 điều trị tại nhà dùng thuốc hạ sốt thế nào?",
                    style: UITextStyle.mediumBlack_18_w700,
                  ),
                  SizedBox(
                    height: 4.0.h,
                  ),
                  Text(
                    """- Đối với người lớn: > 38,5 độ hoặc đau đầu, đau người nhiều: uống mỗi lần 1 viên thuốc hạ sốt như paracetamol 0,5 g, có thể lặp lại mỗi 4-6h, ngày không quá 4 viên, uống oresol nếu ăn kém/giảm hoặc có thể dùng uống thay nước.
    - Đối với trẻ em: > 38,5 độ, uống thuốc hạ sốt như paracetamol liều 10-15 mg/kg/lần, có thể lặp lại mỗi 4-6h, ngày không quá 4 lần.""",
                    style: UITextStyle.mediumBlack_16_w400,
                  ),
                  SizedBox(
                    height: 4.0.h,
                  ),
                  Text(
                    "6. Các dấu hiệu suy hô hấp F0 cần biết",
                    style: UITextStyle.mediumBlack_18_w700,
                  ),
                  SizedBox(
                    height: 4.0.h,
                  ),
                  Text(
                    """- Khó thở, thở hụt hơi, hoặc khó thở tăng lên khi vận động (đứng lên, đi lại trong nhà) hoặc ở trẻ em có dấu hiệu thở bất thường: thở rên, rút lõm lồng ngực, phập phồng cánh mũi, khò khè, thở rít thì hít vào, và/hoặc
    - Nhịp thở (ở trẻ em đếm đủ nhịp thở trong một phút khi trẻ nằm yên không khóc):
      ≥ 20 lần/phút ở người lớn;
      ≥ 30 lần/phút ở trẻ em từ 5 - dưới 12 tuổi;
      ≥ 40 lần/phút ở trẻ em từ 1 đến dưới 5 tuổi;
    Và/hoặc SpO2 ≤ 96% (khi phát hiện bất thường đo lại lần 2 sau 30 giây đến một phút, khi đo yêu cầu giữ yên vị trí đo)""",
                    style: UITextStyle.mediumBlack_16_w400,
                  ),
                  SizedBox(
                    height: 4.0.h,
                  ),
                  Text(
                    "7. 11 dấu hiệu F0 điều trị tại nhà cần được xử trí cấp cứu và chuyển viện kịp thời",
                    style: UITextStyle.mediumBlack_18_w700,
                  ),
                  SizedBox(
                    height: 4.0.h,
                  ),
                  Text(
                    """- Khó thở, thở hụt hơi, hoặc ở trẻ em có dấu hiệu thở bất thường: thở rên, rút lõm lồng ngực, phập phồng cánh mũi, khò khè, thở rít thì hít vào.
    - Nhịp thở
    Người lớn: nhịp thở ≥ 20 lần/phút
    Trẻ từ 1 đến dưới 5 tuổi: Nhịp thở: ≥ 40 lần/phút,
    Trẻ từ 5 – dưới 12 tuổi: nhịp thở: ≥ 30 lần/phút
    (Lưu ý ở trẻ em: đếm đủ nhịp thở trong 1 phút khi trẻ nằm yên không khóc).
    - SpO2 ≤ 96% (trường hợp phát hiện chỉ số SpO2 bất thường cần đo lại lần 2 sau 30 giây đến 1 phút, khi đo yêu cầu giữ yên vị trí đo).
    - Mạch nhanh > 120 nhịp/phút hoặc < 50 nhịp/phút.
    - Huyết áp thấp: huyết áp tối đa < 90 mmHg, huyết áp tối thiểu < 60 mmHg (nếu có thể đo).
    - Đau tức ngực thường xuyên, cảm giác bó thắt ngực, đau tăng khi hít sâu.
    - Thay đổi ý thức: lú lẫn, ngủ rũ, lơ mơ, rất mệt/mệt lả, trẻ quấy khóc, li bì khó đánh thức, co giật.
    - Tím môi, tím đầu móng tay, móng chân, da xanh, môi nhợt, lạnh đầu ngón tay, ngón chân.
    - Không thể uống hoặc bú kém/giảm, ăn kém, nôn (ở trẻ em). Trẻ có biểu hiện hội chứng viêm đa hệ thống: sốt cao, đỏ mắt, môi đỏ, lưỡi dâu tây, ngón tay chân sưng phù nổi hồng ban...
    - Mắc thêm bệnh cấp tính: sốt xuất huyết, tay chân miệng...
    - Bất kỳ tình trạng bất ổn nào của người mắc COVID-19 mà thấy cần báo cơ sở y tế.""",
                    style: UITextStyle.mediumBlack_16_w400,
                  ),
                  SizedBox(
                    height: 4.0.h,
                  ),
                  Text(
                    "8. F0 điều trị tại nhà được dỡ bỏ cách ly khi nào?",
                    style: UITextStyle.mediumBlack_18_w700,
                  ),
                  SizedBox(
                    height: 4.0.h,
                  ),
                  Text(
                    """- Thời gian cách ly, điều trị đủ 7 ngày và kết quả xét nghiệm nhanh kháng nguyên âm tính virus SARS-CoV-2 do nhân viên y tế thực hiện hoặc người bệnh tự thực hiện dưới sự giám sát của nhân viên y tế bằng ít nhất một trong các hình thức trực tiếp hoặc gián tiếp qua các phương tiện từ xa.
    - Trong trường hợp sau 7 ngày kết quả xét nghiệm còn dương tính thì tiếp tục cách ly đủ 10 ngày đối với người đã tiêm đủ liều vaccine theo quy định và 14 ngày đối với người chưa tiêm đủ liều vaccine theo quy định.
    - Trạm Y tế nơi quản lý người bệnh chịu trách nhiệm xác nhận khỏi bệnh cho người bệnh.""",
                    style: UITextStyle.mediumBlack_16_w400,
                  ),
                  SizedBox(
                    height: 4.0.h,
                  ),
                  Text(
                    "9. Vệ sinh nơi ở, xử lý chất thải khi có F0 cách ly, điều trị tại nhà thế nào?",
                    style: UITextStyle.mediumBlack_18_w700,
                  ),
                  SizedBox(
                    height: 4.0.h,
                  ),
                  Text(
                    """- Cần bố trí bộ đồ ăn riêng cho người nhiễm COVID-19, nên sử dụng dụng cụ dùng một lần. Đồ ăn thừa và dụng cụ ăn uống dùng một lần bỏ vào túi đựng rác trong phòng riêng.
    - Rửa bát đĩa bằng nước nóng và xà phòng. Người nhiễm COVID-19 tự rửa bát đĩa trong phòng riêng. Nếu cần người chăm sóc hỗ trợ thì người chăm sóc mang găng khi thu dọn đồ ăn và rửa bát đĩa. Bát đĩa và đồ dùng ăn uống của người nhiễm sau khi rửa để ở vị trí riêng. Tốt nhất là để trong phòng người nhiễm.
    - Về xử lý đồ vải của F0, tốt nhất là người nhiễm có thể tự giặt quần áo của mình. Nếu cần người chăm sóc giặt. Đeo găng tay khi xử lý đồ vải của người nhiễm.
    - Giặt hoặc khử trùng túi giặt và giỏ đồ. Giặt bằng máy hoặc bằng tay với nước ấm nhất có thể. Sấy khô hoặc phơi khô hoàn toàn. Tháo găng, rửa tay sau khi xử lý đồ vải của người nhiễm.
    - Nên giặt riêng đồ của người nhiễm với đồ của người khác. Đặc biệt Bộ Y tế lưu ý "không giũ đồ bẩn cần giặt để hạn chế nguy cơ phát tán vi rút qua không khí"
    - Về vấn đề vệ sinh, tốt nhất là người nhiễm tự vệ sinh khu vực của mình. Để vệ sinh môi trường sạch sẽ cần làm sạch sàn nhà, tường và bề mặt sau đó lau bằng dung dịch khử khuẩn, rồi lau lại bằng nước sạch.
    - Bộ Y tế lưu ý nếu cần người chăm sóc hỗ trợ việc vệ sinh phòng, người chăm sóc mang găng trước khi vệ sinh.""",
                    style: UITextStyle.mediumBlack_16_w400,
                  ),
                  SizedBox(
                    height: 16.0.h,
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 16.0.w),
              child: Text(
                "Các bài viết khác",
                style: UITextStyle.mediumBlack_18_w700,
              ),
            ),
            SizedBox(
              height: 16.0.h,
            ),
            SingleChildScrollView(
              padding: EdgeInsets.symmetric(horizontal: 16.0.w),
              scrollDirection: Axis.horizontal,
              child: IntrinsicHeight(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    GestureDetector(
                      behavior: HitTestBehavior.translucent,
                      onTap: () {
                        Get.to(
                          () => const NewsDetailManualPage(),
                          preventDuplicates: false,
                        );
                      },
                      child: const NewsItemWidget(
                        title: "Hướng dẫn sử dụng máy xông",
                        description: "Hướng dẫn cách sử dụng máy xông EOC cơ bản và các thao tác với ứng dụng EOC",
                        imagePath: "assets/ic_mayxong.jpeg",
                      ),
                    ),
                    SizedBox(
                      width: 16.0.w,
                    ),
                    GestureDetector(
                      behavior: HitTestBehavior.translucent,
                      onTap: () {
                        Get.to(
                          () => const NewsWarningPage(),
                          preventDuplicates: false,
                        );
                      },
                      child: const NewsItemWidget(
                        title: "Các lưu ý khi sử dụng máy xông",
                        description: "Máy xông mặt là thiết bị chăm sóc cá nhân hữu ích nhưng đừng nên lạm dụng nó quá nhiều, mà phải sử dụng máy xông mặt đúng cách.",
                        imagePath: "assets/warning.jpeg",
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 30.0.h,
            ),
          ],
        ),
      ),
    );
  }
}
