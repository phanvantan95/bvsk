import 'package:bvsk/extensions/size_ext.dart';
import 'package:bvsk/screens/prevention/news/news_detail_manual_page.dart';
import 'package:bvsk/screens/prevention/news/news_treatment_page.dart';
import 'package:bvsk/screens/prevention/news_item_widget.dart';
import 'package:bvsk/theme/ui_color.dart';
import 'package:bvsk/theme/ui_text_style.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class NewsWarningPage extends StatelessWidget {
  const NewsWarningPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Stack(
              children: [
                Hero(
                  tag: "assets/warning.jpeg",
                  child: Image(
                    height: 300.0.h,
                    width: double.infinity,
                    image: const AssetImage("assets/warning.jpeg"),
                    fit: BoxFit.fill,
                  ),
                ),
                Container(
                  height: 150.0.h,
                  width: double.infinity,
                  decoration: const BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [
                        UIColor.mediumBlack,
                        Colors.transparent,
                      ],
                    ),
                  ),
                ),
                Positioned(
                  top: MediaQuery.of(context).viewPadding.top,
                  left: 16.0.w,
                  child: Row(
                    children: [
                      IconButton(
                        onPressed: () => Get.back(),
                        icon: Icon(
                          Icons.arrow_back_ios,
                          color: UIColor.white,
                          size: 24.0.h,
                        ),
                      ),
                      SizedBox(
                        width: 4.0.w,
                      ),
                      Text(
                        "Các lưu ý khi sử dụng máy xông",
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: UITextStyle.white_22_w700,
                      )
                    ],
                  ),
                )
              ],
            ),
            SizedBox(
              height: 16.0.h,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 16.0.w),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Máy xông mặt là thiết bị chăm sóc cá nhân hữu ích nhưng đừng nên lạm dụng nó quá nhiều, mà phải sử dụng máy xông mặt đúng cách. ",
                    style: UITextStyle.mediumBlack_16_w400,
                  ),
                  SizedBox(
                    height: 4.0.h,
                  ),
                  Text(
                    "- Chỉ nên xông mặt từ 15 đến 20 phút cho đến khi máy tự ngắt theo chế độ xông mặt của máy. Không nên xông mặt quá 20 phút, để tránh tình trạng da mặt bị quá tải nhiệt và bị tổn thương.",
                    style: UITextStyle.mediumBlack_16_w400,
                  ),
                  SizedBox(
                    height: 4.0.h,
                  ),
                  Text(
                    "- Nên vệ sinh máy thường xuyên. Đổ hết nước dư và dùng khăn ẩm lau sạch.",
                    style: UITextStyle.mediumBlack_16_w400,
                  ),
                  SizedBox(
                    height: 4.0.h,
                  ),
                  Text(
                    "- Không nên cho tinh dầu quá đặc có thể làm hư máy.",
                    style: UITextStyle.mediumBlack_16_w400,
                  ),
                  SizedBox(
                    height: 4.0.h,
                  ),
                  Text(
                    "- Chỉ kết nối bluetooth với 1 thiết bị mỗi ần sử dụng.",
                    style: UITextStyle.mediumBlack_16_w400,
                  ),
                  SizedBox(
                    height: 4.0.h,
                  ),
                  Text(
                    "* Khi có sự cố, hãy nhấn ngay vào nút đỏ bên hông máy. Máy sẽ sự động ngắt tất cả hoạt động.",
                    style: UITextStyle.red_16_w400,
                  ),
                  SizedBox(
                    height: 16.0.h,
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 16.0.w),
              child: Text(
                "Các bài viết khác",
                style: UITextStyle.mediumBlack_18_w700,
              ),
            ),
            SizedBox(
              height: 16.0.h,
            ),
            SingleChildScrollView(
              padding: EdgeInsets.symmetric(horizontal: 16.0.w),
              scrollDirection: Axis.horizontal,
              child: IntrinsicHeight(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    GestureDetector(
                      behavior: HitTestBehavior.translucent,
                      onTap: () {
                        Get.to(
                          () => const NewsDetailManualPage(),
                          preventDuplicates: false,
                        );
                      },
                      child: const NewsItemWidget(
                        title: "Hướng dẫn sử dụng máy xông",
                        description: "Hướng dẫn cách sử dụng máy xông EOC cơ bản và các thao tác với ứng dụng EOC",
                        imagePath: "assets/ic_mayxong.jpeg",
                      ),
                    ),
                    SizedBox(
                      width: 16.0.w,
                    ),
                    GestureDetector(
                      behavior: HitTestBehavior.translucent,
                      onTap: () {
                        Get.to(
                          () => const NewsTreatmentPage(),
                          preventDuplicates: false,
                        );
                      },
                      child: const NewsItemWidget(
                        title: "Hướng dẫn điều trị cho F0",
                        description:
                            "Covid - 19 đã và đang là đại dịch lớn làm ảnh hưởng đến hàng trăm triệu người trên toàn Thế giới. Để hạn chế mức độ lây lan cũng như ảnh hưởng của nó đến sức khỏe, thì ngoài việc tuân thủ các biện pháp 5K chúng ta cần tăng cường sức đề kháng của cơ thể.",
                        imagePath: "assets/treatment.png",
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 30.0.h,
            ),
          ],
        ),
      ),
    );
  }
}
