import 'package:bvsk/extensions/size_ext.dart';
import 'package:bvsk/screens/prevention/news/news_treatment_page.dart';
import 'package:bvsk/screens/prevention/news/news_warning_page.dart';
import 'package:bvsk/screens/prevention/news_item_widget.dart';
import 'package:bvsk/theme/ui_color.dart';
import 'package:bvsk/theme/ui_text_style.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class NewsDetailManualPage extends StatelessWidget {
  const NewsDetailManualPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Stack(
              children: [
                Hero(
                  tag: "assets/ic_mayxong.jpeg",
                  child: Image(
                    height: 300.0.h,
                    width: double.infinity,
                    image: const AssetImage("assets/ic_mayxong.jpeg"),
                    fit: BoxFit.fill,
                  ),
                ),
                Container(
                  height: 150.0.h,
                  width: double.infinity,
                  decoration: const BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [
                        UIColor.mediumBlack,
                        Colors.transparent,
                      ],
                    ),
                  ),
                ),
                Positioned(
                  top: MediaQuery.of(context).viewPadding.top,
                  left: 16.0.w,
                  child: Row(
                    children: [
                      IconButton(
                        onPressed: () => Get.back(),
                        icon: Icon(
                          Icons.arrow_back_ios,
                          color: UIColor.white,
                          size: 24.0.h,
                        ),
                      ),
                      SizedBox(
                        width: 4.0.w,
                      ),
                      Text(
                        "Hướng dẫn sử dụng máy xông",
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: UITextStyle.white_22_w700,
                      )
                    ],
                  ),
                )
              ],
            ),
            SizedBox(
              height: 16.0.h,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 16.0.w),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        height: 8.0.h,
                        width: 8.0.h,
                        decoration: BoxDecoration(
                          color: UIColor.mediumBlack,
                          borderRadius: BorderRadius.circular(4.0.h),
                        ),
                      ),
                      SizedBox(
                        width: 4.0.w,
                      ),
                      Expanded(
                        child: Text(
                          "Hướng dẫn sử dụng máy xông tinh dầu EOC",
                          style: UITextStyle.mediumBlack_18_w700,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 4.0.h,
                  ),
                  Text(
                    "- Cắm dây nguồn vào máy và cắm đầu kia vào nguồn điện, để máy cố định, bằng phẳng.",
                    style: UITextStyle.mediumBlack_16_w400,
                  ),
                  SizedBox(
                    height: 4.0.h,
                  ),
                  Text(
                    "- Nhấc nắp lên để mở, cho nước vào bình chứa (Lượng nước phải trên mức Min và dưới mức Max), khuyến khích sử dụng nước ấm để tăng hiệu quả.",
                    style: UITextStyle.mediumBlack_16_w400,
                  ),
                  SizedBox(
                    height: 4.0.h,
                  ),
                  Text(
                    "- Cho tinh dầu vào (nếu cần).",
                    style: UITextStyle.mediumBlack_16_w400,
                  ),
                  SizedBox(
                    height: 4.0.h,
                  ),
                  Text(
                    "- Đóng nắp và nhấn nút Bắt đầu(Start).",
                    style: UITextStyle.mediumBlack_16_w400,
                  ),
                  SizedBox(
                    height: 8.0.h,
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        height: 8.0.h,
                        width: 8.0.h,
                        decoration: BoxDecoration(
                          color: UIColor.mediumBlack,
                          borderRadius: BorderRadius.circular(4.0.h),
                        ),
                      ),
                      SizedBox(
                        width: 4.0.w,
                      ),
                      Expanded(
                        child: Text(
                          "Hướng dẫn sử dụng máy xông với ứng dụng EOC",
                          style: UITextStyle.mediumBlack_18_w700,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 4.0.h,
                  ),
                  Text(
                    "- Vào mục kết nối, nhấn kết nối với thiết bị EOC.",
                    style: UITextStyle.mediumBlack_16_w400,
                  ),
                  SizedBox(
                    height: 4.0.h,
                  ),
                  Text(
                    "- Nhấn On/off: máy sẽ hoạt động hoặc ngừng.",
                    style: UITextStyle.mediumBlack_16_w400,
                  ),
                  SizedBox(
                    height: 4.0.h,
                  ),
                  Text(
                    "- Nhấn Timer: hẹn giờ hoạt động của máy.",
                    style: UITextStyle.mediumBlack_16_w400,
                  ),
                  SizedBox(
                    height: 4.0.h,
                  ),
                  Text(
                    "- Nhấn turbo: máy hoạt động với công suất lớn.",
                    style: UITextStyle.mediumBlack_16_w400,
                  ),
                  SizedBox(
                    height: 4.0.h,
                  ),
                  Text(
                    "- Nhấn Temperature: điều chỉnh nhiệt độ phù hợp.",
                    style: UITextStyle.mediumBlack_16_w400,
                  ),
                  SizedBox(
                    height: 4.0.h,
                  ),
                  Text(
                    "- Máy sẽ tự động ngắt khi hết nước.",
                    style: UITextStyle.mediumBlack_16_w400,
                  ),
                  SizedBox(
                    height: 16.0.h,
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 16.0.w),
              child: Text(
                "Các bài viết khác",
                style: UITextStyle.mediumBlack_18_w700,
              ),
            ),
            SizedBox(
              height: 16.0.h,
            ),
            SingleChildScrollView(
              padding: EdgeInsets.symmetric(horizontal: 16.0.w),
              scrollDirection: Axis.horizontal,
              child: IntrinsicHeight(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    GestureDetector(
                      behavior: HitTestBehavior.translucent,
                      onTap: () {
                        Get.to(
                          () => const NewsWarningPage(),
                          preventDuplicates: false,
                        );
                      },
                      child: const NewsItemWidget(
                        title: "Các lưu ý khi sử dụng máy xông",
                        description: "Máy xông mặt là thiết bị chăm sóc cá nhân hữu ích nhưng đừng nên lạm dụng nó quá nhiều, mà phải sử dụng máy xông mặt đúng cách.",
                        imagePath: "assets/warning.jpeg",
                      ),
                    ),
                    SizedBox(
                      width: 16.0.w,
                    ),
                    GestureDetector(
                      behavior: HitTestBehavior.translucent,
                      onTap: () {
                        Get.to(
                          () => const NewsTreatmentPage(),
                          preventDuplicates: false,
                        );
                      },
                      child: const NewsItemWidget(
                        title: "Hướng dẫn điều trị cho F0",
                        description:
                            "Covid - 19 đã và đang là đại dịch lớn làm ảnh hưởng đến hàng trăm triệu người trên toàn Thế giới. Để hạn chế mức độ lây lan cũng như ảnh hưởng của nó đến sức khỏe, thì ngoài việc tuân thủ các biện pháp 5K chúng ta cần tăng cường sức đề kháng của cơ thể.",
                        imagePath: "assets/treatment.png",
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 30.0.h,
            ),
          ],
        ),
      ),
    );
  }
}
