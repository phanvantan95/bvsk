import 'package:bvsk/extensions/size_ext.dart';
import 'package:bvsk/screens/prevention/news/news_detail_manual_page.dart';
import 'package:bvsk/screens/prevention/news/news_treatment_page.dart';
import 'package:bvsk/screens/prevention/news/news_warning_page.dart';
import 'package:bvsk/screens/prevention/news_item_widget.dart';
import 'package:bvsk/screens/prevention/prevention_item_widget.dart';
import 'package:bvsk/theme/ui_text_style.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PreventionView extends StatefulWidget {
  const PreventionView({Key? key}) : super(key: key);

  @override
  State<PreventionView> createState() => _PreventionViewState();
}

class _PreventionViewState extends State<PreventionView> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        SizedBox(
          height: 30.0.h,
        ),
        Image(
          height: 50.0.h,
          image: const AssetImage('assets/logo.png'),
          fit: BoxFit.contain,
        ),
        SizedBox(
          height: 30.0.h,
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 16.0.w),
          child: Text(
            "Tài liệu",
            style: UITextStyle.header,
          ),
        ),
        SizedBox(
          height: 12.0.h,
        ),
        SingleChildScrollView(
          padding: EdgeInsets.symmetric(horizontal: 16.0.w),
          scrollDirection: Axis.horizontal,
          child: IntrinsicHeight(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () {
                    Get.to(() => const NewsDetailManualPage());
                  },
                  child: const NewsItemWidget(
                    title: "Hướng dẫn sử dụng máy xông",
                    description: "Hướng dẫn cách sử dụng máy xông EOC cơ bản và các thao tác với ứng dụng EOC",
                    imagePath: "assets/ic_mayxong.jpeg",
                    isHaveHero: true,
                  ),
                ),
                SizedBox(
                  width: 16.0.w,
                ),
                GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () {
                    Get.to(() => const NewsWarningPage());
                  },
                  child: const NewsItemWidget(
                    title: "Các lưu ý khi sử dụng máy xông",
                    description: "Máy xông mặt là thiết bị chăm sóc cá nhân hữu ích nhưng đừng nên lạm dụng nó quá nhiều, mà phải sử dụng máy xông mặt đúng cách.",
                    imagePath: "assets/warning.jpeg",
                    isHaveHero: true,
                  ),
                ),
                SizedBox(
                  width: 16.0.w,
                ),
                GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () {
                    Get.to(() => const NewsTreatmentPage());
                  },
                  child: const NewsItemWidget(
                    title: "Hướng dẫn điều trị cho F0",
                    description:
                        "Covid - 19 đã và đang là đại dịch lớn làm ảnh hưởng đến hàng trăm triệu người trên toàn Thế giới. Để hạn chế mức độ lây lan cũng như ảnh hưởng của nó đến sức khỏe, thì ngoài việc tuân thủ các biện pháp 5K chúng ta cần tăng cường sức đề kháng của cơ thể.",
                    imagePath: "assets/treatment.png",
                    isHaveHero: true,
                  ),
                ),
              ],
            ),
          ),
        ),
        SizedBox(
          height: 8.0.h,
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 16.0.w),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 8.0.h,
                      ),
                      Text(
                        "Covid-19",
                        style: UITextStyle.header,
                      ),
                      SizedBox(
                        height: 8.0.h,
                      ),
                      Text(
                        "Bảo vệ sức khỏe cho bản thân",
                        style: UITextStyle.mediumBlack_16_w400,
                      ),
                    ],
                  ),
                  Image(
                    height: 160.0.h,
                    image: const AssetImage('assets/person.png'),
                    fit: BoxFit.contain,
                  ),
                ],
              ),
              SizedBox(
                height: 40.0.h,
              ),
              Column(
                children: const [
                  PreventionItemWidget(
                    title: "KHẨU TRANG",
                    content: "Đeo khẩu trang vải thường xuyên tại nơi công cộng, nơi tập trung đông người; đeo khẩu trang y tế tại các cơ sở y tế, khu cách ly.",
                    image: 'assets/face_mask.png',
                  ),
                  PreventionItemWidget(
                    title: "KHỬ KHUẨN",
                    content:
                        "Rửa tay thường xuyên bằng xà phòng hoặc dung dịch sát khuẩn tay. Vệ sinh các bề mặt/ vật dụng thường xuyên tiếp xúc (tay nắm cửa, điện thoại, máy tính bảng, mặt bàn, ghế…). Giữ vệ sinh, lau rửa và để nhà cửa thông thoáng.",
                    image: 'assets/wash_hands.png',
                  ),
                  PreventionItemWidget(
                    title: " KHOẢNG CÁCH",
                    content: "Giữ khoảng cách khi tiếp xúc với người khác.",
                    image: 'assets/avoid_contact.png',
                  ),
                  PreventionItemWidget(
                    title: "KHÔNG TỤ TẬP",
                    content: "Không tụ tập đông người",
                    image: 'assets/cover_cough.png',
                  ),
                  PreventionItemWidget(
                    title: "KHAI BÁO Y TẾ",
                    content: "Thực hiện khai báo y tế",
                    image: 'assets/info.jpeg',
                  ),
                ],
              ),
            ],
          ),
        ),
        SizedBox(
          height: 30.0.h,
        )
      ],
    );
  }
}
