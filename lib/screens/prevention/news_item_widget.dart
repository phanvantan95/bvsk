import 'package:bvsk/extensions/size_ext.dart';
import 'package:bvsk/theme/ui_color.dart';
import 'package:bvsk/theme/ui_text_style.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class NewsItemWidget extends StatelessWidget {
  final String? title;
  final String? description;
  final String imagePath;
  final bool? isHaveHero;
  const NewsItemWidget({Key? key, this.title, this.description, required this.imagePath, this.isHaveHero}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width / 2 + 50.0.w,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0.h),
        border: Border.all(color: UIColor.lightGrayBorder),
        color: UIColor.white,
        boxShadow: const [
          BoxShadow(
            color: UIColor.lightShadeGray,
            offset: Offset(1, 2),
            spreadRadius: 1,
          ),
        ],
      ),
      child: Column(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(10.0.h),
              topRight: Radius.circular(10.0.h),
            ),
            child: isHaveHero ?? false
                ? Hero(
                    tag: imagePath,
                    child: Image(
                      height: 180.0.h,
                      width: double.infinity,
                      image: AssetImage(imagePath),
                      fit: BoxFit.fill,
                    ),
                  )
                : Image(
                    height: 180.0.h,
                    width: double.infinity,
                    image: AssetImage(imagePath),
                    fit: BoxFit.fill,
                  ),
          ),
          SizedBox(
            height: 8.0.h,
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 8.0.w),
            child: Text(
              title ?? "",
              style: UITextStyle.mediumBlack_18_w700,
              textAlign: TextAlign.center,
            ),
          ),
          SizedBox(
            height: 8.0.h,
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 12.0.w),
            child: Text(
              description ?? "",
              style: UITextStyle.mediumBlack_14_w400,
              maxLines: 5,
              overflow: TextOverflow.ellipsis,
            ),
          ),
          SizedBox(
            height: 12.0.h,
          ),
        ],
      ),
    );
  }
}
