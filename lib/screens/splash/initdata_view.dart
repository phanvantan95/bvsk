import 'dart:async';

import 'package:bvsk/extensions/size_ext.dart';
import 'package:bvsk/screens/splash/splash_view.dart';
import 'package:bvsk/theme/ui_text_style.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class InitDataView extends StatefulWidget {
  const InitDataView({Key? key}) : super(key: key);

  @override
  State<InitDataView> createState() => _InitDataViewState();
}

class _InitDataViewState extends State<InitDataView> {
  late Timer _timer;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance?.addPostFrameCallback((_) => startTimer());
  }

  void startTimer() {
    const oneSec = Duration(seconds: 1);
    _timer = Timer.periodic(
      oneSec,
      (Timer timer) {
        if (Get.width > 0) {
          _timer.cancel();
          SizeUtil().init();
          UITextStyle.initUITextStyle();
          Get.offAll(() => const SplashView());
        }
      },
    );
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: CircularProgressIndicator(),
    );
  }
}
