import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:bvsk/screens/home/home_view.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:bvsk/extensions/size_ext.dart';
import 'package:bvsk/theme/ui_color.dart';
import 'package:bvsk/theme/ui_text_style.dart';
import 'package:google_fonts/google_fonts.dart';

class SplashView extends StatefulWidget {
  const SplashView({Key? key}) : super(key: key);

  @override
  _SplashViewState createState() => _SplashViewState();
}

class _SplashViewState extends State<SplashView> with TickerProviderStateMixin {
  late AnimationController controller;

  @override
  void initState() {
    super.initState();
    configEasyLoading();
    WidgetsBinding.instance?.addPostFrameCallback((_) => gotoLogin());

    controller = AnimationController(vsync: this, duration: const Duration(seconds: 1));
    controller.forward().then((value) {});
  }

  Future<void> gotoLogin() async {
    await Future.delayed(const Duration(seconds: 2), () {
      Get.off(() => const HomeView());
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: UIColor.white,
      child: Stack(
        children: [
          Column(
            children: [
              SizedBox(
                height: 80.0.h,
              ),
              FadeTransition(
                opacity: controller,
                child: Center(
                  child: Image(
                    height: 60.0.h,
                    image: const AssetImage('assets/logo.png'),
                    fit: BoxFit.contain,
                  ),
                ),
              ),
              SizedBox(
                height: 20.0.h,
              ),
              SizedBox(
                height: Get.height / 4,
              ),
              DefaultTextStyle(
                style: GoogleFonts.openSans(color: UIColor.mediumBlack, fontSize: 40.0.sp, fontWeight: FontWeight.bold),
                child: AnimatedTextKit(
                  totalRepeatCount: 1,
                  animatedTexts: [
                    TyperAnimatedText('EOC', textAlign: TextAlign.center),
                  ],
                ),
              ),
              SizedBox(
                height: 8.0.h,
              ),
              DefaultTextStyle(
                style: GoogleFonts.openSans(color: UIColor.mediumBlack, fontSize: 18.0.sp, fontWeight: FontWeight.bold),
                child: AnimatedTextKit(
                  totalRepeatCount: 1,
                  animatedTexts: [
                    TyperAnimatedText('ESSENTIAL OIL CONTROLLER', textAlign: TextAlign.center),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  void configEasyLoading() {
    EasyLoading.instance
      ..indicatorType = EasyLoadingIndicatorType.fadingCircle
      ..loadingStyle = EasyLoadingStyle.custom
      ..maskType = EasyLoadingMaskType.black
      ..indicatorSize = 60.0.h
      ..radius = 10.0.h
      ..progressWidth = 5.0.h
      ..lineWidth = 10.0.h
      ..backgroundColor = Colors.white
      ..indicatorColor = UIColor.green
      ..textColor = UIColor.mediumBlack
      ..progressColor = UIColor.green
      ..textStyle = UITextStyle.mediumBlack_16_w400
      ..userInteractions = false;
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}
