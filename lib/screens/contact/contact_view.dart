import 'package:bvsk/extensions/size_ext.dart';
import 'package:bvsk/screens/contact/contact_item.dart';
import 'package:bvsk/theme/ui_color.dart';
import 'package:bvsk/theme/ui_text_style.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class ContactView extends StatelessWidget {
  const ContactView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      fit: StackFit.expand,
      children: [
        SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 16.0.w),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                SizedBox(
                  height: 30.0.h,
                ),
                Image(
                  height: 50.0.h,
                  image: const AssetImage('assets/logo.png'),
                  fit: BoxFit.contain,
                ),
                SizedBox(
                  height: 30.0.h,
                ),
                Text(
                  "Thông tin liên hệ",
                  style: UITextStyle.header,
                ),
                SizedBox(
                  height: 20.0.h,
                ),
                const ContactItemWidget(
                  name: "Lê Phước Phương Thảo",
                  email: "ThaoLPPFSC1028@fpt.edu.vn",
                  grade: "10A1",
                ),
                SizedBox(
                  height: 10.0.h,
                ),
                const ContactItemWidget(
                  name: "Huỳnh Thị Mai Hòa",
                  email: "HoaHTMFSC1010@fpt.edu.vn",
                  grade: "10A1",
                ),
                SizedBox(
                  height: 10.0.h,
                ),
                const ContactItemWidget(
                  name: "Nguyễn Thị Quỳnh Giang",
                  email: "GiangNTQFSC114@fpt.edu.vn",
                  grade: "10A1",
                ),
                SizedBox(
                  height: 20.0.h,
                ),
                Text(
                  "*Bảo vệ sức khỏe cho mọi người (đặc biệt là các F0 trong tình hình đại dịch covid 19)",
                  style: UITextStyle.mediumBlack_16_w400.apply(
                    fontStyle: FontStyle.italic,
                  ),
                ),
                SizedBox(
                  height: 40.0.h,
                ),
              ],
            ),
          ),
        ),
        Positioned(
          bottom: 60.0.h,
          right: 20.0.w,
          child: GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () {
              launch("tel://0396535127");
            },
            child: buildIcon(Icons.call),
          ),
        ),
      ],
    );
  }

  Container buildIcon(IconData iconData) {
    return Container(
      height: 60.0.h,
      width: 60.0.h,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: UIColor.primaryColor,
        boxShadow: [
          BoxShadow(
            color: UIColor.mediumBlack.withAlpha(100),
            blurRadius: 4,
            offset: const Offset(0, 4),
          )
        ],
      ),
      child: Icon(
        iconData,
        color: Colors.white,
      ),
    );
  }
}
