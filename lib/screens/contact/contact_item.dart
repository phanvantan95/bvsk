import 'package:bvsk/extensions/size_ext.dart';
import 'package:bvsk/theme/ui_text_style.dart';
import 'package:flutter/material.dart';

class ContactItemWidget extends StatelessWidget {
  final String name;
  final String email;
  final String grade;
  const ContactItemWidget({Key? key, required this.name, required this.email, required this.grade}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          name,
          style: UITextStyle.mediumBlack_16_w700,
        ),
        SizedBox(
          height: 2.0.h,
        ),
        Text(
          "Lớp: $grade",
          style: UITextStyle.mediumBlack_16_w400,
        ),
        SizedBox(
          height: 2.0.h,
        ),
        Text(
          "Email: $email",
          style: UITextStyle.mediumBlack_16_w400,
        ),
      ],
    );
  }
}
