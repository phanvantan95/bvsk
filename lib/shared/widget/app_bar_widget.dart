import 'package:bvsk/theme/ui_color.dart';
import 'package:bvsk/theme/ui_text_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

PreferredSizeWidget buildAppBar(BuildContext context, {String title = '', IconButton? leadingIcon, List<Widget> actions = const []}) {
  return AppBar(
    title: Text(
      title,
      style: UITextStyle.title_20_bold.apply(color: UIColor.white),
    ),
    elevation: 0,
    systemOverlayStyle: SystemUiOverlayStyle.light,
    flexibleSpace: Container(
      decoration: const BoxDecoration(
        color: UIColor.primaryColor,
      ),
    ),
    centerTitle: true,
    leading: leadingIcon,
    actions: actions,
  );
}
