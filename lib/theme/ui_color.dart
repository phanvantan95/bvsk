// ignore_for_file: constant_identifier_names

import 'package:flutter/material.dart';

class UIColor {
  UIColor._();

  static const Color mediumBlack = Color(0xFF272A3F);
  static const Color white = Color(0xFFFFFFFF);
  static const Color green = Color(0xFF088A58);
  static const Color tabbargreen = Color(0xFF088A58);
  static const Color itemgreen = Color(0xFFEBFBEF);
  static const Color lightGreen = Color(0xFF23AF50);
  static const Color green_a20 = Color(0x331F915F);
  static const Color lightBlueBorder = Color(0xFFE2E8F5);
  static const Color lightGrayBorder = Color(0xFFD8DAE0);
  static const Color lightBlueBg = Color(0xFFF4F7FA);
  static const Color lightShadeGray = Color(0xFFE8E8E8);
  static const Color chryslerBurnishedSilver = Color(0xFFC8C8C8);
  static const Color mediumLightShadeGray = Color(0xFFBABABA);
  static const Color gray = Color(0xFF545454);
  static const Color lightGrayShadow_a16 = Color(0x2680828B);
  static const Color grayText = Color(0xFF6E7989);
  static const Color red = Colors.redAccent;
  static const Color background = Color.fromRGBO(245, 245, 245, 1);
  static const Color backgroundBar = Color.fromRGBO(249, 249, 249, 1);
  static const Color backgroundSearch = Color.fromRGBO(202, 201, 207, 1);

  static const Color primaryColor = Color(0xFF0B440A);
  static const Color secondaryColor = Color(0xFF0C8C09);
  static const Color thirdColor = Color(0xFF55D469);
  static const Color fourthColor = Color(0x3953D060);
  static const Color fifthColor = Color(0xFFE5E5E5);
  static const Color sixthColor = Color(0xFFC4C4C4);
  static const Color sevenColor = Color(0xFF506ED7);
}
