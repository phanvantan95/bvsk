// ignore_for_file: non_constant_identifier_names

import 'package:flutter/material.dart';
import 'package:bvsk/extensions/size_ext.dart';
import 'package:bvsk/theme/ui_color.dart';

class UITextStyle {
  UITextStyle._();
  static initUITextStyle() {
    UITextStyle.white_8_w400 = TextStyle(color: UIColor.white, fontSize: 8.0.sp, fontWeight: FontWeight.w400);
    UITextStyle.mediumBlack_8_w400 = TextStyle(color: UIColor.mediumBlack, fontSize: 8.0.sp, fontWeight: FontWeight.w400);

    UITextStyle.white_10_w400 = TextStyle(color: UIColor.white, fontSize: 10.0.sp, fontWeight: FontWeight.w400);
    UITextStyle.mediumBlack_10_bold = TextStyle(color: UIColor.mediumBlack, fontSize: 10.0.sp, fontWeight: FontWeight.bold);

    UITextStyle.mediumLightShadeGray_12_w400 = TextStyle(color: UIColor.mediumLightShadeGray, fontSize: 12.0.sp, fontWeight: FontWeight.w400);
    UITextStyle.gray_12_w400 = TextStyle(color: UIColor.gray, fontSize: 12.0.sp, fontWeight: FontWeight.w400);
    UITextStyle.mediumLightShadeGray_12_w700 = TextStyle(color: UIColor.mediumLightShadeGray, fontSize: 12.0.sp, fontWeight: FontWeight.w700);
    UITextStyle.red_12_w400 = TextStyle(color: UIColor.red, fontSize: 12.0.sp, fontWeight: FontWeight.w400);
    UITextStyle.mediumBlack_12_w400 = TextStyle(color: UIColor.mediumBlack, fontSize: 12.0.sp, fontWeight: FontWeight.w400);
    UITextStyle.mediumBlack_12_w700 = TextStyle(color: UIColor.mediumBlack, fontSize: 12.0.sp, fontWeight: FontWeight.w700);
    UITextStyle.mediumBlack_12_bold = TextStyle(color: UIColor.mediumBlack, fontSize: 12.0.sp, fontWeight: FontWeight.bold);
    UITextStyle.white_12_w400 = TextStyle(color: UIColor.white, fontSize: 12.0.sp, fontWeight: FontWeight.w400);

    UITextStyle.mediumBlack_13_w400 = TextStyle(color: UIColor.mediumBlack, fontSize: 13.0.sp, fontWeight: FontWeight.w400);

    UITextStyle.red_14_w400 = TextStyle(color: UIColor.red, fontSize: 14.0.sp, fontWeight: FontWeight.w400);
    UITextStyle.gray_14_w400 = TextStyle(color: UIColor.gray, fontSize: 14.0.sp, fontWeight: FontWeight.w400);
    UITextStyle.mediumBlack_14_w400 = TextStyle(color: UIColor.mediumBlack, fontSize: 14.0.sp, fontWeight: FontWeight.w400);
    UITextStyle.white_14_w400 = TextStyle(color: UIColor.white, fontSize: 14.0.sp, fontWeight: FontWeight.w700);
    UITextStyle.mediumBlack_14_w700 = TextStyle(color: UIColor.mediumBlack, fontSize: 14.0.sp, fontWeight: FontWeight.w700);
    UITextStyle.green_14_w400 = TextStyle(color: UIColor.green, fontSize: 14.0.sp, fontWeight: FontWeight.w400);
    UITextStyle.mediumLightShadeGray_14_w400 = TextStyle(color: UIColor.mediumLightShadeGray, fontSize: 14.0.sp, fontWeight: FontWeight.w400);
    UITextStyle.mediumLightShadeGray_14_w700 = TextStyle(color: UIColor.mediumLightShadeGray, fontSize: 14.0.sp, fontWeight: FontWeight.w700);
    UITextStyle.red_14_w700 = TextStyle(color: UIColor.red, fontSize: 14.0.sp, fontWeight: FontWeight.w700);
    UITextStyle.white_14_w700 = TextStyle(color: UIColor.white, fontSize: 14.0.sp, fontWeight: FontWeight.w700);
    UITextStyle.orange_14_w400 = TextStyle(color: Colors.orangeAccent, fontSize: 14.0.sp, fontWeight: FontWeight.w400);

    UITextStyle.green_15_w400 = TextStyle(color: UIColor.green, fontSize: 15.0.sp, fontWeight: FontWeight.w400);
    UITextStyle.mediumBlack_15_w400 = TextStyle(color: UIColor.mediumBlack, fontSize: 15.0.sp, fontWeight: FontWeight.w400);
    UITextStyle.mediumBlack_15_w700 = TextStyle(color: UIColor.mediumBlack, fontSize: 15.0.sp, fontWeight: FontWeight.w700);

    UITextStyle.mediumBlack_16_w400 = TextStyle(color: UIColor.mediumBlack, fontSize: 16.0.sp, fontWeight: FontWeight.w400);
    UITextStyle.grayText_16_w400 = TextStyle(color: UIColor.grayText, fontSize: 16.0.sp, fontWeight: FontWeight.w400);
    UITextStyle.lightGreen_16_w400 = TextStyle(color: UIColor.lightGreen, fontSize: 16.0.sp, fontWeight: FontWeight.w400);
    UITextStyle.white_16_w400 = TextStyle(color: UIColor.white, fontSize: 16.0.sp, fontWeight: FontWeight.w400);
    UITextStyle.green_16_w400 = TextStyle(color: UIColor.green, fontSize: 16.0.sp, fontWeight: FontWeight.w400);
    UITextStyle.red_16_w400 = TextStyle(color: Colors.red, fontSize: 16.0.sp, fontWeight: FontWeight.w400);
    UITextStyle.orange_16_w400 = TextStyle(color: Colors.orangeAccent, fontSize: 16.0.sp, fontWeight: FontWeight.w400);
    UITextStyle.gray_16_w400 = TextStyle(color: UIColor.gray, fontSize: 16.0.sp, fontWeight: FontWeight.w400);
    UITextStyle.mediumLightShadeGray_16_w400 = TextStyle(color: UIColor.mediumLightShadeGray, fontSize: 16.0.sp, fontWeight: FontWeight.w400);
    UITextStyle.chryslerBurnishedSilver_16_w400 = TextStyle(color: UIColor.chryslerBurnishedSilver, fontSize: 16.0.sp, fontWeight: FontWeight.w400);
    UITextStyle.lightGreen_16_w700 = TextStyle(color: UIColor.lightGreen, fontSize: 16.0.sp, fontWeight: FontWeight.w700);
    UITextStyle.white_16_w700 = TextStyle(color: UIColor.white, fontSize: 16.0.sp, fontWeight: FontWeight.w700);
    UITextStyle.mediumBlack_16_w700 = TextStyle(color: UIColor.mediumBlack, fontSize: 16.0.sp, fontWeight: FontWeight.w700);
    UITextStyle.green_16_w700 = TextStyle(color: UIColor.green, fontSize: 16.0.sp, fontWeight: FontWeight.w700);
    UITextStyle.red_16_w700 = TextStyle(color: UIColor.red, fontSize: 16.0.sp, fontWeight: FontWeight.w700);
    UITextStyle.orange_16_w700 = TextStyle(color: Colors.orangeAccent, fontSize: 16.0.sp, fontWeight: FontWeight.w700);
    UITextStyle.grayText_16_w700 = TextStyle(color: UIColor.grayText, fontSize: 16.0.sp, fontWeight: FontWeight.w700);

    UITextStyle.grayText_17_w700 = TextStyle(color: UIColor.grayText, fontSize: 17.0.sp, fontWeight: FontWeight.w700);
    UITextStyle.green_17_w700 = TextStyle(color: UIColor.green, fontSize: 17.0.sp, fontWeight: FontWeight.w700);
    UITextStyle.green_17_bold = TextStyle(color: UIColor.green, fontSize: 17.0.sp, fontWeight: FontWeight.bold);

    UITextStyle.mediumBlack_18_w400 = TextStyle(color: UIColor.mediumBlack, fontSize: 18.0.sp, fontWeight: FontWeight.w400);
    UITextStyle.white_18_w700 = TextStyle(color: UIColor.white, fontSize: 18.0.sp, fontWeight: FontWeight.w700);
    UITextStyle.mediumBlack_18_w700 = TextStyle(color: UIColor.mediumBlack, fontSize: 18.0.sp, fontWeight: FontWeight.w700);
    UITextStyle.gray_18_w700 = TextStyle(color: UIColor.gray, fontSize: 18.0.sp, fontWeight: FontWeight.w700);
    UITextStyle.green_18_w700 = TextStyle(color: UIColor.green, fontSize: 18.0.sp, fontWeight: FontWeight.w700);

    UITextStyle.green_20_w700 = TextStyle(color: UIColor.green, fontSize: 20.0.sp, fontWeight: FontWeight.w700);

    UITextStyle.green_22_w700 = TextStyle(color: UIColor.green, fontSize: 22.0.sp, fontWeight: FontWeight.w700);
    UITextStyle.red_22_w700 = TextStyle(color: UIColor.red, fontSize: 22.0.sp, fontWeight: FontWeight.w700);
    UITextStyle.red_22_w400 = TextStyle(color: UIColor.red, fontSize: 22.0.sp, fontWeight: FontWeight.w400);
    UITextStyle.white_22_w700 = TextStyle(color: UIColor.white, fontSize: 22.0.sp, fontWeight: FontWeight.w700);
    UITextStyle.mediumBlack_22_w700 = TextStyle(color: UIColor.mediumBlack, fontSize: 22.0.sp, fontWeight: FontWeight.w700);
    UITextStyle.lightGreen_22_w700 = TextStyle(color: UIColor.lightGreen, fontSize: 22.0.sp, fontWeight: FontWeight.w700);
    UITextStyle.gray_22_w700 = TextStyle(color: UIColor.gray, fontSize: 22.0.sp, fontWeight: FontWeight.w700);

    UITextStyle.green_24_w700 = TextStyle(color: UIColor.green, fontSize: 24.0.sp, fontWeight: FontWeight.w700);
    UITextStyle.mediumBack_24_w700 = TextStyle(color: UIColor.mediumBlack, fontSize: 24.0.sp, fontWeight: FontWeight.w700);
    UITextStyle.red_24_w700 = TextStyle(color: UIColor.red, fontSize: 24.0.sp, fontWeight: FontWeight.w700);

    UITextStyle.green_28_w700 = TextStyle(color: UIColor.green, fontSize: 28.0.sp, fontWeight: FontWeight.w700);
    UITextStyle.white_28_w700 = TextStyle(color: UIColor.white, fontSize: 28.0.sp, fontWeight: FontWeight.w700);
    UITextStyle.red_28_w700 = TextStyle(color: UIColor.red, fontSize: 28.0.sp, fontWeight: FontWeight.w700);
    UITextStyle.mediumBlack_28_w700 = TextStyle(color: UIColor.mediumBlack, fontSize: 28.0.sp, fontWeight: FontWeight.w700);

    UITextStyle.title = TextStyle(color: UIColor.mediumBlack, fontSize: 50.0.sp, fontWeight: FontWeight.bold);
    UITextStyle.title_20_bold = TextStyle(color: UIColor.mediumBlack, fontSize: 20.0.sp, fontWeight: FontWeight.bold);
    UITextStyle.header = TextStyle(color: UIColor.mediumBlack, fontSize: 24.0.sp, fontWeight: FontWeight.bold);
    UITextStyle.header_small = TextStyle(color: UIColor.mediumBlack, fontSize: 16.0.sp, fontWeight: FontWeight.bold);
    UITextStyle.body = TextStyle(color: UIColor.mediumBlack, fontSize: 14.0.sp, fontWeight: FontWeight.normal);
    UITextStyle.body_bold = TextStyle(color: UIColor.mediumBlack, fontSize: 14.0.sp, fontWeight: FontWeight.bold);
    UITextStyle.body_small = TextStyle(color: UIColor.mediumBlack, fontSize: 12.0.sp, fontWeight: FontWeight.normal);
  }

  static late TextStyle white_8_w400;
  static late TextStyle mediumBlack_8_w400;

  static late TextStyle white_10_w400;
  static late TextStyle mediumBlack_10_bold;

  static late TextStyle mediumLightShadeGray_12_w400;
  static late TextStyle mediumLightShadeGray_12_w700;
  static late TextStyle red_12_w400;
  static late TextStyle mediumBlack_12_w400;
  static late TextStyle mediumBlack_12_w700;
  static late TextStyle mediumBlack_12_bold;
  static late TextStyle white_12_w400;
  static late TextStyle gray_12_w400;

  static late TextStyle mediumBlack_13_w400;

  static late TextStyle red_14_w400;
  static late TextStyle gray_14_w400;
  static late TextStyle mediumBlack_14_w400;
  static late TextStyle white_14_w400;
  static late TextStyle mediumBlack_14_w700;
  static late TextStyle green_14_w400;
  static late TextStyle mediumLightShadeGray_14_w400;
  static late TextStyle mediumLightShadeGray_14_w700;
  static late TextStyle red_14_w700;
  static late TextStyle white_14_w700;
  static late TextStyle orange_14_w400;

  static late TextStyle green_15_w400;
  static late TextStyle mediumBlack_15_w400;
  static late TextStyle mediumBlack_15_w700;

  static late TextStyle mediumBlack_16_w400;
  static late TextStyle grayText_16_w400;
  static late TextStyle lightGreen_16_w400;
  static late TextStyle white_16_w400;
  static late TextStyle green_16_w400;
  static late TextStyle red_16_w400;
  static late TextStyle orange_16_w400;
  static late TextStyle gray_16_w400;
  static late TextStyle mediumLightShadeGray_16_w400;
  static late TextStyle chryslerBurnishedSilver_16_w400;
  static late TextStyle lightGreen_16_w700;
  static late TextStyle white_16_w700;
  static late TextStyle mediumBlack_16_w700;
  static late TextStyle green_16_w700;
  static late TextStyle red_16_w700;
  static late TextStyle orange_16_w700;
  static late TextStyle grayText_16_w700;

  static late TextStyle grayText_17_w700;
  static late TextStyle green_17_w700;
  static late TextStyle green_17_bold;

  static late TextStyle mediumBlack_18_w400;
  static late TextStyle white_18_w700;
  static late TextStyle mediumBlack_18_w700;
  static late TextStyle gray_18_w700;
  static late TextStyle green_18_w700;

  static late TextStyle green_20_w700;

  static late TextStyle green_22_w700;
  static late TextStyle red_22_w700;
  static late TextStyle red_22_w400;
  static late TextStyle white_22_w700;
  static late TextStyle mediumBlack_22_w700;
  static late TextStyle lightGreen_22_w700;
  static late TextStyle gray_22_w700;

  static late TextStyle mediumBack_24_w700;
  static late TextStyle green_24_w700;
  static late TextStyle red_24_w700;

  static late TextStyle green_28_w700;
  static late TextStyle white_28_w700;
  static late TextStyle red_28_w700;
  static late TextStyle mediumBlack_28_w700;

  static late TextStyle title;
  static late TextStyle header;
  static late TextStyle header_small;
  static late TextStyle body;
  static late TextStyle body_bold;
  static late TextStyle body_small;
  static late TextStyle title_20_bold;
}
